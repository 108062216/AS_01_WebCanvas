# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

![](https://i.imgur.com/aG2BLIM.jpg)
從最左邊開始，被黑框圍起來的部分就是我們的畫布，可以在右邊工作區選取你想要的工具在畫布上作畫。
**Color**底下的button是調色盤，**Line Width**底下的bar是筆刷的粗細程度，可自己調整。
**Pen**的話就是基本的筆刷，**Eraser**是橡皮擦。
接下來的三個可以直接畫出特定的形狀，**Circle**可以拿來畫圓，**Triangle**可以拿來畫三角形，**Rectangle**可以拿來畫正方形。
點選**Text**的部分可以幫助你在畫布上Type一些文字上去，旁邊的**Type Face**跟**Word Size**可以讓你分別挑選想要的字型及字體大小。
點選**Undo**可以回到上一步，點**Redo**則可回復剛剛被Undo的動作，但在Undo之後若有新的動作，就不能Redo回去了。
**Refresh**的話則是刷新整個畫布。
**Upload**可以讓你選擇你想要的檔案上傳到畫布上，但不會改變畫布大小(單純不喜歡...)。而**Download**則可以把你的圖畫給下載下來。


### Function description
![](https://i.imgur.com/pGmOwnS.jpg)

我有令一個Flag叫tool，tool可能為Pen,Eraser,text,Circle,Triangle,Rectangle其一。

![](https://i.imgur.com/r07id2T.jpg)
get_xy是拿來抓x,y的位置。
start_draw, drawing, stop_draw 分別表示用筆刷和橡皮擦時的點下滑鼠、移動滑鼠、放開滑鼠(or超出畫布)。

![](https://i.imgur.com/RYQ76M8.jpg)
different_brush_start, different_brush_drawing, different_brush_stop這三個是負責畫出特定形狀(圓、三角、正方形)時的function，概念上跟一般筆刷很像，分別對應著start_draw, drawing, stop_draw這三個function。

![](https://i.imgur.com/sSTs39I.jpg)
draw_text的部分負責在畫布上輸入，handleEnter監測當user按下"Enter"鍵後確定文字後Type到畫布上。

![](https://i.imgur.com/XgebEnL.jpg)
Undo、Redo、Refresh分別就做button被點到時的工作。

![](https://i.imgur.com/khRbk0A.jpg)
Upload跟Download時的function，handleImage負責監控當有圖片被選擇Upload上來時開始動作。



### Gitlab page link
https://108062216.gitlab.io/AS_01_WebCanvas


### Others (Optional)
No, thanks Teacher and TAs!

<style>
table th{
    width: 100%;
}
</style>