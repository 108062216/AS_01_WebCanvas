const canvas = document.getElementById("myCanvas");
canvas.width = window.innerWidth*0.6;
canvas.height = window.innerHeight*0.85;
var ctx = canvas.getContext("2d");

var Canvas_bg_color = "white";
ctx.fillStyle = Canvas_bg_color;
ctx.fillRect(0, 0, canvas.width, canvas.height);

ctx.lineCap = "round";
ctx.font = "20px serif";
var pen_color = "black";
var pen_width = "50";

var pre_x = 0, cur_x = 0, pre_y = 0, cur_y = 0;
var is_drawing = false;
var can_redo = 0;
var txt_has_Input = false;

var last_img;
var different_brush_is_drawing = false;

const img_circle = new Image();
img_circle.src = "circle.png";
const img_triangle = new Image();
img_triangle.src = "triangle.png";
const img_rectangle = new Image();
img_rectangle.src = "rectangle.png";


var diff_x=0, diff_y=0;


var image_array = [];
var index_images = 0;
image_array[0] = ctx.getImageData(0, 0, canvas.width, canvas.height);


canvas.addEventListener("click", draw_text, false);

canvas.addEventListener("mousedown", start_draw, false);
canvas.addEventListener("mouseup", stop_draw, false);
canvas.addEventListener("mousemove", drawing, false);
canvas.addEventListener("mouseout", stop_draw, false);

canvas.addEventListener("mousedown", different_brush_start, false);
canvas.addEventListener("mouseup", different_brush_stop, false);
canvas.addEventListener("mousemove", different_brush_drawing, false);
canvas.addEventListener("mouseout", different_brush_stop, false);

var tool = "Pen";   // "Eraser", "Text"
function Pen(){
    tool = "Pen";
    canvas.style.cursor = "url('pencil.png') ,auto";
}
function Eraser(){
    tool = "Eraser";
    canvas.style.cursor = "url('eraser.png') ,auto";
}
function text(){
    tool = "text";
    canvas.style.cursor = "text";
}
function Circle(){
    tool = "Circle";
    canvas.style.cursor = "url('circle_cursor.png') ,auto";
}
function Triangle(){
    tool = "Triangle";
    canvas.style.cursor = "url('triangle_cursor.png') ,auto";
}
function Rectangle(){
    tool = "Rectangle";
    canvas.style.cursor = "url('rectangle_cursor.png') ,auto";
}


function get_xy(Pen){
    pre_x = cur_x;
    pre_y = cur_y;
    cur_x = Pen.clientX - 47;
    //console.log(canvas.offsetLeft);
    cur_y = Pen.clientY - 44;
}

function start_draw(Pen) {
    if(tool === "Pen")
    {
        ctx.globalCompositeOperation = "source-over";
    }
    else if(tool === "Eraser")
    {
        ctx.globalCompositeOperation = "destination-out";
    }
    else return;

    is_drawing = true;
    ctx.strokeStyle = pen_color;
    ctx.lineWidth = pen_width;
    ctx.lineCap = "round";
    ctx.clear
    get_xy(Pen);

    //to draw a dot
    ctx.beginPath();
    ctx.moveTo(cur_x, cur_y);
    ctx.lineTo(cur_x, cur_y);
    ctx.stroke();
    ctx.closePath();

    //to draw a path
    ctx.beginPath();
}

function drawing(Pen) {
    if(is_drawing)
    {
        get_xy(Pen);
        ctx.moveTo(pre_x, pre_y);
        ctx.lineTo(cur_x, cur_y);
        ctx.stroke();
    }
}

function stop_draw(Pen){
    if(tool!=="Pen" && tool!=="Eraser") return;

    ctx.closePath();

    if(Pen.type != "mouseout" || is_drawing)
    {  
        index_images += 1;
        console.log("stop_draw+1");
        image_array[index_images] = ctx.getImageData(0, 0, canvas.width, canvas.height);
        can_redo = 0;
    }
    
    is_drawing = false;
}


function Undo(){
    if(index_images > 0)
    {
        index_images -= 1;
        ctx.putImageData(image_array[index_images], 0, 0);
        can_redo += 1;
    }
}
function Redo(){
    if(can_redo > 0)
    {
        can_redo -= 1;
        index_images += 1;
        ctx.putImageData(image_array[index_images], 0, 0);
    }
}
function Refresh(){
    var r = confirm("Sure to refresh?");
    if (r == true) {
        ctx.fillStyle = Canvas_bg_color;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillRect(0, 0, canvas.width, canvas.height);

        image_array = [];
        index_images = 0;
    }
}

function Upload(){
    var Up_button = document.createElement("input");
    Up_button.type = "file";
    Up_button.click();
    
    Up_button.addEventListener('change', handleImage, false);
}
function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            /*canvas.width = img.width;
            canvas.height = img.height;*/
            ctx.drawImage(img,0,0);
            index_images += 1;
            console.log("upload+1");
            image_array[index_images] = ctx.getImageData(0, 0, canvas.width, canvas.height);
            can_redo = 0;
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
    
}

function Download(event){
    image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream");
    var link = document.createElement('a');
    link.download = "canvas.png";
    link.href = image;
    link.click();
}


function draw_text(event) {
    if(tool !== "text") return;
    if(txt_has_Input) return;
    var input = document.createElement('input');
    get_xy(event);

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (cur_x + 22) + 'px';
    input.style.top = (cur_y + 20) + 'px';

    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();

    txt_has_Input = true;
}
function handleEnter(event) {
    var KeyCode = event.keyCode;
    if (KeyCode === 13) {
        var TypeFace = document.getElementById("TypeFace").value;
        var WordSize = document.getElementById("WordSize").value;

        ctx.font = WordSize+'px '+TypeFace;
        ctx.fillStyle = pen_color;
        ctx.fillText(this.value, cur_x-16, cur_y+5);
        document.body.removeChild(this);
        txt_has_Input = false;

        index_images += 1;
        console.log("text+1");
        image_array[index_images] = ctx.getImageData(0, 0, canvas.width, canvas.height);
        can_redo = 0;
    }
}

function different_brush_start(Pen){
    if(tool!=="Circle" && tool!=="Triangle" && tool!=="Rectangle") return;
    ctx.globalCompositeOperation = "source-over";
    ctx.strokeStyle = pen_color;
    ctx.lineWidth = pen_width;

    get_xy(Pen);
    diff_x = cur_x;
    diff_y = cur_y;

    different_brush_is_drawing = true;
}
function different_brush_drawing(Pen){
    if(different_brush_is_drawing)
    {
        if(tool!=="Circle" && tool!=="Triangle" && tool!=="Rectangle") return;
        get_xy(Pen);

        ctx.putImageData(image_array[index_images], 0, 0);
        ctx.beginPath();

        if(tool=="Circle")
        {
            var radius = Math.sqrt((cur_x - diff_x)*(cur_x - diff_x)+(cur_y - diff_y)*(cur_y - diff_y));
            ctx.arc(diff_x, diff_y, radius, 0, 2 * Math.PI);
            ctx.stroke();
        }
        else if(tool=="Triangle")
        {
            ctx.moveTo(diff_x, diff_y);
            ctx.lineTo(cur_x, cur_y);
            ctx.moveTo(cur_x, cur_y);
            ctx.lineTo(diff_x-(cur_x-diff_x), cur_y);
            ctx.moveTo(diff_x-(cur_x-diff_x), cur_y);
            ctx.lineTo(diff_x, diff_y);
            ctx.stroke();
        }
        else if(tool=="Rectangle")
        {
            ctx.strokeRect(diff_x, diff_y, cur_x-diff_x, cur_y-diff_y);
        }

        ctx.closePath();
    }
}
function different_brush_stop(Pen){
    if(tool!=="Circle" && tool!=="Triangle" && tool!=="Rectangle") return;
    if(Pen.type != "mouseout" || different_brush_is_drawing)
    {  
        index_images += 1;
        console.log("circle+1");
        image_array[index_images] = ctx.getImageData(0, 0, canvas.width, canvas.height);
        can_redo = 0;
    }
    different_brush_is_drawing = false;
}